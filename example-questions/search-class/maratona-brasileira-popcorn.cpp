#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int,int> ii;

const int INF = 0x3f3f3f3f;
const ll LINF = 0x3f3f3f3f3f3f3f3fll;  

int N,C,T;
int pipocas[MAX];

bool possivel(long long chute) {
    
    int comp =1;
    long long resta = T*chute;
    for (int i = 0; i < N; i++) {
        if (resta >= pipocas[i]) {
            resta -= pipocas[i];
        }else {
            comp++;
            resta = T*chute;
            i--;
        }
        if(comp > C) return 0;
    }
}

int32_t main(){
ios_base::sync_with_stdio(0);cin.tie(0);
    int n; cin >> n;
    cin >> N >> C >> T;
    for (int i = 0; i < N; i++) {
        cin >> pipocas[i];
    }
    int l=0,r=1e9+1;
    while(l < r) {
        int m = (l+r)/2;

        if (!possivel(m)) {
            l=m+1;
        }
        else {
            r=m;
        }
    }
    cout << l << endl;
exit(0);
}
