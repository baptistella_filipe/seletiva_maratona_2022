// essa é uma quest�o  na qual se pede :
// Dadas 4 listas de n�meros de tamanho n cada (A,B,C,D), queremos encontrar quantas quadruplas 
// (i,j,k,l) (0<=,j,k,l<n) existem tal que A[i] + B[j] + C[k] + D[l] == 0

// Solution with O(n4), worst solution
for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
        for (int k = 0; k < n; k++) {
            for (int w = 0; w < n; w++) {
                if (a[i]+b[j]+c[k]+d[w] == 0) {
                        ans++;
                }
            }
        }
    }
}

// Solution with O(n2 log n) 
// encontrar todas as somas para os pares (i,j) e todas as somas para os pares (k,l).
// Encontra a resposta para o problema contando quantos pares de valores (v,-v) existe tal que v
// está na primeira solu��o e -v está na segunda

long long ans =0;
vector<long long> ab, cd;
for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
        ab. push_back(a[i]+b[j]);
        cd. push_back(c[i]+d[j]);
    }
}
sort(cd.begin(), cd.end());
for (int i = 0; i < (int)ab.size(); i++) {
    auto l = lower_bound(cd.begin(),cd.end(), -ab[i]);
    auto r = upper_bound(cd.begin(),cd.end(), -ab[i]);
    ans += (r-l);
}

