// this is a binary search algorithm 
//  in which, we could find the number with O(log(n))
//  n == vector.size()
//
int binary_search( vector<int> &A, int k) {
    int low = 0, high = A.size()-1;
    while(low <= high) {
        int mid = (low+high)/2
        if (k==A[mid]) {
            return mid;
        }   else if (k < A[mid]) {
            high= mid -1;
        }else {
            low = mid +1;
        } 
    }
    return -1;

}
// lower_bound : first element of the container which isnt smaller than the give element
//
// upper_bound : first element of the container which is greater than the give element
//
// ( map and set)
