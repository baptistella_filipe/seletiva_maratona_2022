#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int,int> ii;

const int INF = 0x3f3f3f3f;
const ll LINF = 0x3f3f3f3f3f3f3f3fll;  

int n,m, a[20000];
string s,t;
bool take[20000];
bool check(int mid) {
    for (int i = 0; i <=mid; i++) {
        take[a[i]] = true;
    }
    int i=0,j=0;
    while(i<n) {
        if (take[i]) {
            i++;
            continue;
        }
        if (s[i]==t[j]) {
            i++;j++;    
        }
        else i++;
        if(j==m) return true;
    }
    int l=0, r=n;
    int ans=n;
    while(l<=r) {
        int mid = (l+r)/2;
        if (check(mid) {
            l = mid+1;
            ans=mid;
        }else r=mid-1;
    }
    cout << ans;
    return 0;
}

int32_t main(){
ios_base::sync_with_stdio(0);cin.tie(0);
    cin >> s>>t;
    n = s.size();
    m = t.size();
    for (int i = 0; i < n; i++) {
        cin>>a[i];
        a[i]--;
    }

exit(0);
}
