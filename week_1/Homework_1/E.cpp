#include <bits/stdc++.h>

using namespace std;

typedef long ll ;
//typedef v.begin(),v.end() v.all();
int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    int n;
    
    while(cin >> n && n != 0){
        map <vector <int>, int> subjects;
        while (n--){
            vector<int> frosh(5);
            for (int i = 0; i < 5; i++){
                int x;
                cin >> x;
                frosh.push_back(x);
            } sort(frosh.begin(), frosh.end());
            if(subjects.count(frosh)==1){
				subjects[frosh]++;
			    }
                else{
                    subjects[frosh]=1;
                }
        }int m = -1;
        for (auto a: subjects){
            m = max(m,a.second);
        }
        int count =0;
        for(auto a: subjects){
            if(a.second == m){
                count++;
            }
        }
        m = m*count;
        cout << m << endl; 
    }
    
    return 0;
}