#include <bits/stdc++.h>

using namespace std;

typedef long ll ;
//typedef v.begin(),v.end() v.all();
int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n;
    vector < pair<int,bool> > evento;
    cin >> n;
    while(n--){
        int a, b;
        cin >> a >> b;
        evento.push_back({a, 0});
        evento.push_back({b, 1});
    }
    sort(evento.begin(),evento.end());
    int count =0, m =-1;
    for(auto a: evento){
        int temp = a.first;
        bool tipo = a.second;
        if(tipo == 0){
            count++;
            m = max(m,count);
        }else{
            count--;
        }
    }
    if(m == 1 || m == 2)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
    return 0;
}