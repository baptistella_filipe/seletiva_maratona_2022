#include <bits/stdc++.h> 

using namespace std;
typedef long long ll;

int main(){
    ios::sync_with_stdio(0);cin.tie(0);
    int operations;
    cin >> operations;
    stack< int> gifts;
    while(operations--){
        string o;
        int x;
        cin >> o;
        if(o == "PUSH"){
            cin >> x;
            if(gifts.empty()){
                gifts.push(x);
            }else {
                if(gifts.top() > x){
                    gifts.push(x);
                }else{
                    gifts.push(gifts.top());
                }
            } 
        }else if(o == "MIN"){
            if(gifts.empty()) cout << "EMPTY\n";
            else{
                cout << gifts.top()<< "\n";
            } 
        }else if( o == "POP"){
            if(gifts.empty()) cout << "EMPTY\n";
            else gifts.pop();
        }
    }
    return 0;
}