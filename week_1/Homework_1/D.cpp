#include <bits/stdc++.h>

using namespace std;

int main(){
    int n;
    while(cin >> n && n != 0){
        int flag =1, k =1;
        stack <int> narrow;        
        for (int i = 1; i <= n;i++){
            int x;
            cin >> x;
            //cout << x;
            if(x == k) k++;
            else{  
                if(!narrow.empty()){                   
                    if(narrow.top() < x && narrow.top()!=k){
                        flag =0;
                        //break;
                    }else if(narrow.top() == k){
                        while( !narrow.empty() && narrow.top() == k){
                            narrow.pop();
                            k++;
                        }
                        if(narrow.empty()) narrow.push(x);
                        else {
                            if(narrow.top() < x){
                                flag = 0;
                                //break;
                            }
                            else narrow.push(x);
                        }
                    }else if(narrow.top() > x) narrow.push(x);
                } else narrow.push(x);
            }
        }
        //cout << n << endl;
        if(flag) cout << "yes" << endl;
        else cout << "no"<< endl;
        //continue;
    }
    return 0;
}