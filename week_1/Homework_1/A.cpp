#include <bits/stdc++.h>

using namespace std;


int flag_p=1, flag_q =1, flag_s=1;

void insert(int x, priority_queue<int>& p,queue<int>& q,stack<int>& s){
    p.push(x);
    s.push(x);
    q.push(x);
}

void check(int x, priority_queue<int>& p,queue<int>& q,stack<int>& s){
    if(s.empty() || s.top() != x){
        flag_s =0;
    }else{
         s.pop();
    }
    if(p.empty() || p.top() != x){
        flag_p =0;
    }else{
        p.pop();
    }
    if(q.empty() || q.front() != x){
        flag_q =0;
    }else{
        q.pop();
    }
}
void print_flag(){
    if(!flag_p && !flag_q && !flag_s){
        cout << "impossible" <<endl;
    }else if(flag_p && !flag_q && !flag_s){
        cout << "priority queue" << endl;
    }else if(!flag_p && !flag_q && flag_s){
        cout << "stack" << endl;
    }else if(!flag_p && flag_q && !flag_s){
        cout << "queue" << endl;
    }else{
        cout << "not sure" << endl;
    }
}

int main() {
    int n;
    while(cin >> n){
        flag_p =1; flag_q=1, flag_s=1;
        priority_queue <int> p;
        queue <int> q;
        stack <int> s;
        while(n--){
            int operation, x;
            cin >> operation >> x;

            if(operation == 1){
                insert(x, p,q,s);
            }else{
                check(x, p,q,s);
            }
        }
        print_flag();
    }
    
    return 0;
}