#include <bits/stdc++.h>

using namespace std;

typedef long ll ;
#define endl "\n"

int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int c;
    cin >> c;
    while (c--){
        int l,m;
        int voltas=0;
        cin >> l >> m;
        queue <int> left;
        queue <int> right;
        while (m--){
            string side;
            int car_length;
            cin >> car_length >> side;
            
            if (side == "left"){
                left.push(car_length);
            }else{
                right.push(car_length);
            }
        }int up=0;
        l *=100;
        bool time = false;
        while (!left.empty() || !right.empty())
        {
            up = 0;
            if (time){
                while (!right.empty() && up + right.front() <= l){
                        up += right.front(); right.pop();
                }
            }
            else{
                while (!left.empty() && up + left.front() <= l){
                        up += left.front(); left.pop();
                }
            }
            voltas++;
            if(time == false){
                time = true;
            }else{
                time = false;
            }
        }
        cout << voltas << endl;

    }
    
    return 0;
}