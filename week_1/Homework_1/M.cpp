#include <bits/stdc++.h>

using namespace std;

int main(){  
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int t;
    cin >> t;
    while (t--){
        int n, a;
        cin >> n;
        vector<int> coins;
        while(n--){   
            cin >> a;
            coins.push_back(a);
        }
        int sum = 0;
        int n_coins = 1;
        if(coins.size() == 1){
            cout << n_coins << endl;
        }else{
            for (int i=0; i < coins.size()-1; i++){
                if (sum + coins[i] < coins[i+1]){
                    sum += coins[i];
                    //cout << coins[i] << " ";
                    n_coins++;
                }
            }
            cout << n_coins << endl;
        }
        
    }
    return 0;
}