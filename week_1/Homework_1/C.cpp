#include <bits/stdc++.h>

using namespace std;

typedef long ll ;
//typedef v.begin(),v.end() v.all();
int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n;
    cin >> n;
    list < int> queue;
    int reverse =0;
    while (n--){
        string s;
        cin >> s;
        
        if(s == "back"){
            if(queue.empty()) cout << "No job for Ada?" << endl;
            else{
                if(!reverse){
                    cout << queue.back() << endl;
                    queue.pop_back();
                }else{
                    cout << queue.front() << endl;
                    queue.pop_front();
                }
            } 

        }else if(s == "front"){
            if(queue.empty()) cout << "No job for Ada?" << endl;
            else{
                if(!reverse){
                    cout << queue.front() << endl;
                    queue.pop_front();
                }else{
                    cout << queue.back() << endl;
                    queue.pop_back();
                }
            } 
        }else if( s == "reverse"){
            if(reverse) reverse =0;
            else reverse =1; 
        }else if(s == "push_back" ){
            int x;
            cin >> x;
            if(!reverse) queue.push_back(x);
            else queue.push_front(x);
        }else if(s == "toFront"){
            int x;
            cin >> x;
            if(!reverse) queue.push_front(x);
            else queue.push_back(x);
        }
    }
    
    return 0;
}