#include <bits/stdc++.h>

using namespace std;

int main(){
    int n;
    while (cin >> n && n) {
        vector <int> final(n);
        while (true) {
            cin >> final[0];
            if (final[0] == 0){
                cout << endl;
                break;
            }
            for (int i = 1; i < n; i++) {
                cin >> final[i];
            }
            stack <int> station_narrow;
            int k = 0;
            for(int i = 1; i<=n; i++){
                station_narrow.push(i);
                while(!station_narrow.empty() and station_narrow.top() == final[k]){
                    station_narrow.pop();
                    k++;
                    if(k == n) break;
                }
            }
            if(station_narrow.empty()) cout << "Yes" << endl;
            else cout << "No" << endl;
        }
    }

	return 0;
}