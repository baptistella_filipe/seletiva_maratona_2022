#include <bits/stdc++.h>

using namespace std;

typedef long long ll ;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n;
    double sum=0;
    double a ,b;
    cin >> n;
    
    for (int i = 0; i < n; i++){
        
        cin >> a >> b;

        int resp = round(log2(b/a)*12); 
        sum += resp;
    }
    cout << abs(sum);
    return 0;
}