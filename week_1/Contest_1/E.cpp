#include <bits/stdc++.h>

using namespace std;

int solve(string s){

    int tam = s.size();
    if(tam % 2 == 0){
        string a = s.substr(0,tam/2);
        string b = a;
        //if(tam/2 %2 == 0) reverse(b.begin(), b.end());
        
        string final = a+b;
       // cout << final << " " << s  << endl;
        if (final == s) return 1;
        else return 0;
    }else return 0;
}

int main() {
    int t;
    cin >> t;
    string s;

    while(t--){
        cin >> s;

        if( !solve(s) ) cout << "NO" << endl;
        else cout << "YES" << endl;
    }
    
    return 0;
}