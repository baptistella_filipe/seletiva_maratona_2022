#include <bits/stdc++.h>

using namespace std;

typedef long ll ;
//typedef v.begin(),v.end() v.all();
#define endl "\n"

int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n;
    cin >> n;
    int small =3, large = 7;
    map <int,int> ans;
    while (n--){   
        int x;
        for(int i=0; i<=14; i++){
            for(int j=0; j<=33; j++){
                ans[small*i+large*j]=1;

            }
        }
        ans[0]=0;
        while(cin>>x){ 
            if(!ans[x]) cout<<"NO"<<endl;  
            else cout<<"YES"<<endl;
        }
    }
    return 0;
}