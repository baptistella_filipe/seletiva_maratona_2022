#include <bits/stdc++.h>

using namespace std;

int fat (int n) {

    if ((n==1) || (n==0)) return 1;               
    else return fat(n-1)*n;
}

int solve(int n){

    int combina = fat(2+n-1);
    combina /= fat(n);
    return combina -2;

}

int main() {
    int n;
    cin >> n;
    cout << (n-1) << endl; 
    
    return 0;
}

/*
    1 23456
    11 1111
    111 111
    1111 11
    11111 1

    1 111111
    11 11111
    111 1111
    1111 111
    11111 11
    111111 1
*/
