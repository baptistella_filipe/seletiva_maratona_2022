#include <bits/stdc++.h>

using namespace std;

int main(){
	ios_base::sync_with_stdio(0);cin.tie(0);
	int n;
	int streak;
	cin >> n >> streak;
	int j = 0;
	vector <int> alturas(n);
	for (int i = 0; i < n; ++i){
		cin >> alturas[i];
    }
    int sum = 0;
	for (int i = 0; i < streak; ++i){
		sum += alturas[i];
    }
    int indice = 0;
    int aux = sum;
	for (int i = streak; i < n; ++i){
		aux += alturas[i] - alturas[j];

		if(aux < sum){
			sum = aux;
			indice = j + 1;
		}
		j++;
	}
	
	cout << indice + 1 <<'\n';

	return 0;
}