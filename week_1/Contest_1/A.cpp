#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int,int> ii;

const int INF = 0x3f3f3f3f;
const ll LINF = 0x3f3f3f3f3f3f3f3fll;  

int32_t main(){
ios_base::sync_with_stdio(0);cin.tie(0);
    int t; cin >> t;
    for (int i = 0; i < t; i++) {
        int n; cin>> n;
        deque<int> deck;
        for (int j = 0; j < n; j++) {
            int e; cin >> e;
            if (deck. front() > e) {
                deck.push_front(e);
            }else {
                deck. push_back(e);
            }
        }
        for(auto a: deck) {
            cout << a << " ";
        }
        cout << endl;
    }
exit(0);
}
