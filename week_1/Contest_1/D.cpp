#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define f first
#define s second
#define pb push_back

typedef long long ll;
typedef pair<int,int> ii;

const int INF = 0x3f3f3f3f;
const ll LINF = 0x3f3f3f3f3f3f3f3fll;  

int32_t main(){
ios_base::sync_with_stdio(0);cin.tie(0);
    int t; cin >> t;
    cin.ignore();
    while(t--) {
        string line;
        getline(cin, line);
        stack <int> pil;
        bool flag =1;
        for (int i = 0; i < line.size(); i++) {
            if (line[i] == '(' || line[i] == '['){
                pil.push(line[i]);
            }else if( line[i] == ']') {
                if (!pil.empty() && pil.top() == '[') {
                    pil.pop();
                }else {
                    cout << "No" << endl;
                    flag =0;
                    break;
                }
            }else if (line [i] == ')') {
                 if (!pil.empty() && pil.top() == '(') {
                    pil.pop();
                }else {
                    cout << "No" << endl;
                    flag =0;
                    break;
                }
            }
        }if(flag){
            if (pil.empty()) {
                cout << "Yes " << endl;
            }else {
                cout << "No" << endl;
            }
        }
    }
exit(0);
}
